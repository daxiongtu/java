### 创建一个基于 maven 的 java 应用项目命令：
```
mvn archetype:generate -DgroupId=com.companyname.bank -DartifactId=consumerBanking -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
```

### 构建项目
打开命令控制台，跳转到 consumerBanking 目录下，并执行以下 mvn 命令开始构建项目：
```
mvn clean package
```

### 执行
进入到consumerBanking\target\classes 目录执行命令 java com.companyname.bank.App
