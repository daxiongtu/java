#!/bin/sh

# 对于需要依赖的jar包使用-cp ，
jar_path='./jars'
jar_files=''
java_files=' '

function get_files() {
	dir_name=$1
	for file in `ls ${dir_name}`
	do
		suffix="${file##*.}"

		file_path=$dir_name'/'$file

		if [[ -f $file_path ]]; then
			if [[ $suffix == 'java' ]]; then
				java_files=${java_files}' '${file_path}

			elif [[ $suffix == 'jar' ]]; then
				# 多个jar包之间使用冒号：java命令运行时注意.:
				jar_files=${jar_files}':'${file_path}
			fi
		elif [[ -d $file_path && $suffix == 'jars' ]]; then
			get_files $file_path
		fi
	done
}


get_files '.'

main_java="HelloWorld"

echo "Compile Jave Source Code"
javac -classpath ${jar_files} ${java_files}

echo "Run...."
java -classpath ${jar_files} ${main_java}
