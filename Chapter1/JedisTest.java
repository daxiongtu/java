import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.BinaryClient;
import java.util.*;


class JedisTest {

	/**
     * sorted set 是有序集合,它在 set 的基础上增加了一个顺序属性,这一属性在添加修 改元素的时候可以指定,每次指定后,会自动重新按新的值调整顺序。
     * 可以理解了有两列的 mysql 表,一列存 value,一列存顺序。
     * sort set和set类型一样，也是string类型元素的集合，也没有重复的元素，不同的是sort set每个元素都会关联一个权，
     * 通过权值可以有序的获取集合中的元素添加，删除，查找的复杂度都是O(1)
     */
	void testSortSet(Jedis jedis) {
		System.out.println(jedis.flushDB());
		jedis.zadd("sortKey", 300, "abel");
		jedis.zadd("sortKey", 20, "mysql");
		jedis.zadd("sortKey", 40, "redis");

		// 按权值从小到大排序
		System.out.println(jedis.zrange("sortKey", 0, -1));
		// 按权值从大到小排序
		System.out.println(jedis.zrevrange("sortKey", 0, -1));

		// 元素个数
		System.out.println("元素个数: " + jedis.zcard("sortKey"));

		// 元素abel的下标
		System.out.println("元素abel的小标: " + jedis.zscore("sortKey", "abel"));

		// 删除元素abel
		// jedis.zrem("sortKey", "abel");
		// System.out.println(jedis.zrange("sortKey", 0, -1));

		// 权值0-100的总数
		System.out.println("0-100的总数: " + jedis.zcount("sortKey", 0, 100));
		// 给元素redis的权值+ 50
		System.out.println("给元素的权值+50：" + jedis.zincrby("sortKey", 50, "redis"));
		// 权值在0-100的值
		System.out.println("权值在0-100的值：" + jedis.zrangeByScore("sortKey", 0, 100));

		// 返回mysql的权值的排名，从0开始计数
		System.out.println(jedis.zrank("sortKey", "mysql"));

		// 输出整个集合的值
		System.out.println("输出整个集合值: " + jedis.zrange("sortKey", 0, -1));
	}


	 /**
     * set 是无序集合,最大可以包含(2 的 32 次方-1)40多亿个元素。
     * set 的是通过 hash table 实现的, 所以添加,删除,查找的复杂度都是 O(1)。hash table 会随着添加或者删除自动的调整大小。
     */
    void testSet(Jedis jedis) {
    	jedis.sadd("person", "abel1");
    	jedis.sadd("person", "abel2");
    	jedis.sadd("person", "abel3");
    	jedis.sadd("person", "abel4");
    	jedis.sadd("person", "abel4");

    	// 获取所有加入的value
    	System.out.println(jedis.smembers("person"));

    	// 从person 移出abel4
    	jedis.srem("person", "abel4");

    	// 获取所有加入的value
    	System.out.println("values: " + jedis.smembers("person"));

    	// 判断abels是否是person集合的元素
    	System.out.println(jedis.sismember("person", "abels"));

    	// 返回集合中的一个随机元素
    	System.out.println(jedis.srandmember("person"));

    	// 返回集合的元素个数
    	System.out.println(jedis.scard("person"));
    }

	// list 是一个链表结构,可以理解为一个每个子元素都是 string 类型的双向链表
	void testList(Jedis jedis) {
		// 移出lists中的所有内容
		jedis.del("lists");

		// 向key lists链表头部添加字符串元素
		jedis.lpush("lists", "abel1");
		jedis.lpush("lists", "abel2");
		jedis.lpush("lists", "abel3");

		// 向key lists链表尾部添加字符串元素
		jedis.rpush("lists", "abel4");
		jedis.rpush("lists", "abel5");

		// 获取lists的长度
		System.out.println(jedis.llen("lists"));

		// 按顺序输出链表中所有的元素
		System.out.println(jedis.lrange("lists", 0, -1));

		// 在abel4 前插入ablList
		// jedis.linsert("lists", BinaryClient.LIST_POSITION.BEFORE, "abel4", "abelLinsert");
		// System.out.println(jedis.lrange("lists", 0, -1));

		// 修改列表指定小标2的值
		jedis.lset("lists", 2, "abelLset");
		System.out.println(jedis.lrange("lists", 0, -1));

		// 插入两个abel2为了后面的删除
		jedis.lpush("lists", "abel2");
		jedis.lpush("lists", "abel2");

		// 从lists中删除3个value=abel2的元素，可以不连续
		// 单删除count=0 个时，ze删除全部value=abel2的元素
		jedis.lrem("lists", 0, "abel2");
		System.out.println(jedis.lrange("lists", 0, -1));
	}
	void testMap(Jedis jedis) {
		Map<String, String> map = new HashMap<>();
		map.put("address", "上海");
		map.put("name", "abel");
		map.put("age", "23");
		jedis.hmset("user", map);

		// 从map中取出value
		// 第一个参数是存入redis中map对象的key，后面跟的是放入map中的对象的key，后面的key可以跟多个，是可变
		List<String> getmap1 = jedis.hmget("user", "address");
		List<String> getmap2 = jedis.hmget("user", "address", "age");
		System.out.println(getmap1);
		System.out.println(getmap2);

		// 删除map中的某个键值
		jedis.hdel("user", "age");

		// 返回key为user的键中存放的值的个数
		System.out.println(jedis.hlen("user"));

		// 是否存在key为user的记录
		System.out.println(jedis.exists("user"));

		// 返回map对象中的所有的key
		System.out.println("all keys: " + jedis.hkeys("user"));

		// 返回map对象中的所有value
		System.out.println("all values: " + jedis.hvals("user"));

		// 获取user中所有的key
		Set<String> keys = jedis.hkeys("users");
		keys.stream().forEach(x -> System.out.println("key: " + x));

	}
	void testFunc(Jedis jedis) {
		// clear
		jedis.del("key");
		jedis.del("name");
		jedis.del("age");
		jedis.del("qq");

		// set
		jedis.set("key", "20181231");
		jedis.mset("name", "abel", "age", "23", "qq", "123244232");
		String name = jedis.get("name");
		String age = jedis.get("age");
		String qq = jedis.get("qq");

		// print
		String value = jedis.get("key");
		System.out.println(value);
		System.out.println(name);
		System.out.println(age);
		System.out.println(qq);

		//age  + 1
        jedis.incr("age");
	}
	public void test() {
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxIdle(8);
		config.setMaxTotal(18);
		JedisPool pool = new JedisPool(config, "127.0.0.1", 6379, 2000);
		Jedis jedis = pool.getResource();


        // testMap(jedis);
        // testList(jedis);
        testSortSet(jedis);

		jedis.close();
		pool.close();
	}
}
