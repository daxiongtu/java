
class FreshJuice {
	enum FreshJuiceSize {SMALL, MEDIUM, LARGE}
	FreshJuiceSize size;
}
public class HelloWorld {
	public static void main(String[] args) {
		System.out.println("Hello world");

		FreshJuice juice = new FreshJuice();
		juice.size = FreshJuice.FreshJuiceSize.MEDIUM;

		JedisTest jedisTest = new JedisTest();
		jedisTest.test();
	}
}