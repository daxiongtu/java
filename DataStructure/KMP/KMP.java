public class KMP{

	public static void main(String[] args) {
		testAlgorithm();
	}

	public static void testAlgorithm() {
		boolean flag = contains("hello world", "hello");
		System.out.println("===>" + flag);
	}
	public static int[] getNext(String target) {
		char[] tData = target.toCharArray();
		int[] next = new int[target.length()];
		next[0] = 0;

		int i = 1;
		int k = 0;

		while(i < tData.length){
			if (i == 1) {
				next[i] = 0;
				i++;
				continue;
			}
			if (k == 0) {
				if (tData[k] == tData[i-1]) {
					k++;
					next[i] = i;
					i++;
				} else {
					next[i] = 0;
					i++;
				}
				continue;
			}
			if (tData[k] == tData[i-1]) {
				if (tData[k+1] != tData[i]) {
					k++;
					next[i] = k;
					i++;
				} else {
					next[i] = next[k];
					i++;
					k = next[k];
				}
			} else {
				k = next[k];
			}
		}
		return next;
	}

	public static boolean contains(String source, String target) {
		if (target == null || source == null) {
			return false;
		}

		char[] sData = source.toCharArray();
		char[] tData = target.toCharArray();

		if (tData.length > sData.length) {
			return false;
		}

		int[] next = getNext(target);
		int i = 0;
		int j = 0;

		while(i < sData.length && j < tData.length) {
			if (sData[i] == tData[i]) {
				j++;
				i++;
			} else {
				if (j == 0) {
					i++;
				}
				j = next[j];
			}
		}

		if (j == tData.length) {
			return true;
		}
		return false;
	}

}