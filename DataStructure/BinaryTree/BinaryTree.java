class TreeNode {
	// 左节点
	private TreeNode leftTreeNode;

	// 右节点
	private TreeNode rightTreeNode;

	// 数据
	private int value;

	public int getValue(){
		return this.value;
	}

	public TreeNode(int value) {
		this.value = value;
	}
	public void setLeftNode(TreeNode node) {
		this.leftTreeNode = node;
	}
	public void setRightNode(TreeNode node) {
		this.rightTreeNode = node;
	}
	public TreeNode getLeftTreeNode() {
		return this.leftTreeNode;
	}

	public TreeNode getRightTreeNode() {
		return this.rightTreeNode;
	}
}

class TreeRoot{
	private TreeNode treeRoot;
	public TreeNode getTreeRoot(){
		return treeRoot;
	}

	public void setTreeRoot(TreeNode treeRoot) {
		this.treeRoot = treeRoot;
	}
}



public class BinaryTree {

	public static void main(String[] args) {
		
		// TreeNode rootTreeNode = initBinaryTree();
		// preTraverseBTree(rootTreeNode);
		testBinarySearchTree();

	}
	public static void testBinarySearchTree() {
		int[] arrays = {2, 3, 1, 4, 5, 100, 20, 79, 887, 887, 2288};

	    //动态创建树

	    TreeRoot root = new TreeRoot();
	    for (int value : arrays) {
	        createTree(root, value);
	    }

	    //先序遍历树
	    preTraverseBTree(root.getTreeRoot());
	    System.out.println("--------------");

	    //中序遍历树
	    inTraverseBTree(root.getTreeRoot());
	    System.out.println("--------------");


	    int height = getHeight(root.getTreeRoot());
	    System.out.println("二叉查找树的高度为:" + height);

	    int max = getMax(root.getTreeRoot());
	    System.out.println("二叉查找树的最大值为:" + max);

	}
	/* 
	 * 动态创建二叉查找树
	 * treeRoot 根节点
	 */
	public static void createTree(TreeRoot treeRoot, int value) {
		//  如果，二叉查找树根为空（第一次访问），将第一个值作为根节点
		if (treeRoot.getTreeRoot() == null) {
			TreeNode treeNode = new TreeNode(value);
			treeRoot.setTreeRoot(treeNode);
		} else {
			// 当前二叉树根
			TreeNode tempRoot = treeRoot.getTreeRoot();

			while (tempRoot != null) {
				// 当前值大于根，往右走
				if (value > tempRoot.getValue()) {
					// 右边没有树根，就直接插入节点
					if (tempRoot.getRightTreeNode() == null) {
						tempRoot.setRightNode(new TreeNode(value));
						return;
					} else {
						// 如果右边有树根，到右边树根去
						tempRoot = tempRoot.getRightTreeNode();
					}

				} else {
					// 左边没有树根，就直接插入节点
					if (tempRoot.getLeftTreeNode() == null) {
						tempRoot.setLeftNode(new TreeNode(value));
						return;
					} else {
						// 如果左边有树根，则，到左边的树根去
						tempRoot = tempRoot.getLeftTreeNode();
					}
				}
			}
		}
	}
	public static TreeNode initBinaryTree() {
		//根节点-->10
        TreeNode treeNode1 = new TreeNode(10);

        //左孩子-->9
        TreeNode treeNode2 = new TreeNode(9);

        //右孩子-->20
        TreeNode treeNode3 = new TreeNode(20);
        
        //20的左孩子-->15
        TreeNode treeNode4 = new TreeNode(15);
        
        //20的右孩子-->35
        TreeNode treeNode5 = new TreeNode(35);


        // 根节点的左右孩子
        treeNode1.setLeftNode(treeNode2);
        treeNode1.setRightNode(treeNode3);

        // 20节点的左右孩子
        treeNode3.setLeftNode(treeNode4);
        treeNode3.setRightNode(treeNode5);
        return treeNode1;
	}
	// 先序遍历
	public static void preTraverseBTree(TreeNode rootTreeNode) {
		if (rootTreeNode != null) {
			// 访问根节点
			System.out.println(rootTreeNode.getValue());

			// 访问左节点
			preTraverseBTree(rootTreeNode.getLeftTreeNode());

			// 访问右节点
			preTraverseBTree(rootTreeNode.getRightTreeNode());
		}
	}
	// 中序遍历
	public static void inTraverseBTree(TreeNode rootTreeNode) {
		if (rootTreeNode != null) {
			// 访问左节点
			inTraverseBTree(rootTreeNode.getLeftTreeNode());

			// 访问根节点
			System.out.println(rootTreeNode.getValue());

			// 访问右节点
			inTraverseBTree(rootTreeNode.getRightTreeNode());
		}
	}

	// 后序遍历
	public static void trialTraverseBTree(TreeNode rootTreeNode) {
		if (rootTreeNode != null) {
			// 访问右节点
			inTraverseBTree(rootTreeNode.getRightTreeNode());

			// 访问左节点
			inTraverseBTree(rootTreeNode.getLeftTreeNode());

			// 访问根节点
			System.out.println(rootTreeNode.getValue());
		}
	}

	public static int getHeight(TreeNode rootTreeNode) {
		if (rootTreeNode == null) {
			return 0;
		} else {
			// 左子树的深度
			int left = getHeight(rootTreeNode.getLeftTreeNode());

			// 右子树的深度
			int right = getHeight(rootTreeNode.getRightTreeNode());
			int max = left;
			if (right > max) {
				max = right;
			}
			return max + 1;
		}
	}

	public static int getMax(TreeNode rootTreeNode) {
		if (rootTreeNode == null){
			return -1;
		} else {
			// 找出左子树的最大值
			int left = getMax(rootTreeNode.getLeftTreeNode());

			// 找出右子树的最大值
			int right = getMax(rootTreeNode.getRightTreeNode());

			int currentValue = rootTreeNode.getValue();

			int max = left;
			if (right > max) {
				max = right;
			}

			if (currentValue > max) {
				max = currentValue;
			}
			return max;

		}
	}

}