import groovy.json.JsonSlurper;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
 
public class JsonUrlParser { 
    public static void main(String[] args) throws IOException, MalformedURLException {
        parserVersion();
        parserJSON();
    }

    // 解析版本号：从0开始至最后一个_之间的数据
    static void parserVersion() {
        def version = "StarMaker_9.9.9_Release_801909251125_dc8688039d"
        println(version);
        int index = 0;
        int position = 0;
        for (ch in version) {
            if (ch == '_' ) {
                position = index;
            }
            index += 1;
        }
        position -= 1
        version = version[0..position]
        println(version)
    }
    static void parserJSON() {
        String JSON_URL = "https://www.mrhaki.com/samples/sample.json";
        URL url = new URL(JSON_URL);
        InputStream urlStream = null;
        try {
            urlStream = url.openStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlStream));
            JsonSlurper jsonSlurper = new JsonSlurper();
            Object result = jsonSlurper.parse(reader);
 
            assert result instanceof Map
            Map jsonResult = (Map) result;
            Map user = (Map) jsonResult.get("user");
            String name = (String) user.get("name");
            Integer age = (Integer) user.get("age");
            List interests = (List) user.get("interests");
 
            assert name.equals("mrhaki");
            assert age == 38;
            assert interests.size() == 2;
            assert interests.get(0).equals("Groovy");
            assert interests.get(1).equals("Grails");

            println(interests)
 
        } finally {
            if (urlStream != null) {
                urlStream.close();
            }
        }
    }
}